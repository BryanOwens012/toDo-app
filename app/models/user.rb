class User < ApplicationRecord
	has_many :openTasks
	validates :name, presence: true
	validates :email, presence: true
end
