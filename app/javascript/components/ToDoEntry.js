import React from "react"
import PropTypes from "prop-types"

class ToDoEntry extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			task: props.task,
		};
	}  

  render () {
    return (
     	<tr>
     		<td> {this.state.task.taskID} </td>
     		<td> {this.state.task.content} </td>
     	</tr>
    );
  }
}

ToDoEntry.defaultProps = {
	task : {taskID: 100, content: "test content"}
}

export default ToDoEntry
