import React from "react"
import PropTypes from "prop-types"
class BoxAddTodo extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			 content: "",
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this); 
	}

	handleChange(event) {
		var value = event.target.value;

		this.setState(function () {
			return {
				content: value
			}
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		console.log(this.state.content);
	}


  render () {
    return (
    	<form onSubmit = {this.handleSubmit}>
        	<input
        		id = "content"
        		placeholder = "click to add a to-do"
        		type = "text"
        		onChange = {this.handleChange}
        	/>
        	<button
        		className = "button"
        		type = "submit"
        		disabled = {!this.state.content}
        	 >
        		Submit
        	</button>
        </form>
    );
  }
}

export default BoxAddTodo
