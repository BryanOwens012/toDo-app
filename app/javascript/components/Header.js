import React from "react";
import PropTypes from "prop-types";
class Header extends React.Component {
  render () {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>Thanks for visiting my ToDo app. Click below to get started.</h2>
      </div>
    );
  }
}

export default Header