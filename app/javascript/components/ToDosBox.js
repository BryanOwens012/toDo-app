import React from "react"
import PropTypes from "prop-types"
import ToDoEntry from './ToDoEntry';
import BoxAddTodo from './BoxAddTodo';

class ToDosBox extends React.Component {
  render () {
    return (
      <div>
      	<table border="1">
      		<tbody>
	    		<ToDoEntry />
	    		<ToDoEntry />
	    	</tbody>
        </table>
        <BoxAddTodo />
      </div>
    );
  }
}

export default ToDosBox