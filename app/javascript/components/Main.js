import React from "react";
import PropTypes from "prop-types";
import Header from './Header';
import Footer from './Footer';
import ToDosBox from './ToDosBox';

var styles = {
	footer: {
		textAlign: 'center',
		fontSize: '60 px'
	}
}; 


class Main extends React.Component {
  render () {
    return (
      <div>
        <Header />
        <ToDosBox />
        <Footer style = {styles.footer}  />
      </div>   
      );
  }
}

export default Main
