class RemoveTimeCreatedFromOpenTasks < ActiveRecord::Migration[5.1]
  def change
    remove_column :open_tasks, :timeCreated, :string
  end
end
