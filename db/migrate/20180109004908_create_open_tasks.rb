class CreateOpenTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :open_tasks do |t|
      t.integer :taskID
      t.string :timeCreated
      t.text :content
      t.integer :taskIsClosed

      t.timestamps
    end
  end
end
