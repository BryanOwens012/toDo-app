class AddTimeDeletedToOpenTasks < ActiveRecord::Migration[5.1]
  def change
    add_column :open_tasks, :timeDeleted, :datetime
  end
end
