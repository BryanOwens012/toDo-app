class RemoveTimeCreatedFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :timeCreated, :datetime
  end
end
