require 'test_helper'

class OpenTaskTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
  	@user = User.new(name: "Example User", email: "user@example.com")
  	@task = OpenTask.new(user: @user, content: "Content")
  end

  test "should be valid" do
  	assert @task.valid?
  end

  test "task content should be present" do
  	@task.content = ""
  	assert_not @task.valid?
  end

end
